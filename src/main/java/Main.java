package com.socgen;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        UserDAO userDAO =new UserDAO<User>();
        System.out.println("Please Choose The Option");
        Scanner scan=new Scanner(System.in);
        System.out.println("1.Add User");
        System.out.println("2.Delete User");
        System.out.println("3.Display User");
        int option= scan.nextInt();
        switch (option){
            case 1:
                System.out.println("Please Enter The Type Of User");
                System.out.println("1.Visitor");
                System.out.println("2.Employee");
                System.out.println("3.Customer");
                int userType=scan.nextInt();
                switch (userType)
                {
                    case 1:
                        System.out.println("Please Enter The Visitor Information");
                        System.out.println("User First Name");
                        String FirstName= scan.next();
                        System.out.println("User Last Name");
                        String LastName=scan.next();
                        System.out.println("User Feedback");
                        String feedback=scan.next();
                        userDAO.add(new Visitors(FirstName,LastName,feedback));
                        break;
                    case 2:
                        System.out.println("Please Enter The Employee Details");
                        System.out.println("User First Name");
                        String firstName=scan.next();
                        System.out.println("User Last Name");
                        String lastName=scan.next();
                        System.out.println("Employee Id");
                        int EmpId=scan.nextInt();
                        System.out.println("Employee Password");
                        String EmpPassword=scan.next();
                        userDAO.add(new Employee(firstName,lastName,EmpId,EmpPassword));
                        break;
                    case 3:
                        System.out.println("Please Enter The Customer Information");
                        System.out.println("User First Name");
                        FirstName=scan.next();
                        System.out.println("User Last Name");
                        LastName=scan.next();
                        System.out.println("Customer Id");
                        int customerId=scan.nextInt();
                        System.out.println("Customer Password");
                        String customerPassword=scan.next();
                        userDAO.add(new Customer(FirstName,LastName,customerId,
                                customerPassword));

                }
        }



    }

}

